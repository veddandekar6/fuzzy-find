// Called when the user clicks on the browser action.
function inject() {
  chrome.tabs.executeScript({ file: "scripts/mark.es6.min.js" });
  chrome.tabs.executeScript({ file: "scripts/viewFinder.js" });
  chrome.tabs.executeScript({ file: "scripts/highlightScript.js" });
}
