ABSTRACT 
---------

The internet is cluttered with data. Although more data is always a good
thing, there is an emerging need to segregate and classify relevant data
from junk.

Modern day Information Retrieval systems such as search engines work
only at the surface level, presenting us with entire pages for a query.
Searching within these pages is a tedious task that is left up to the
user. Though search within documents is supported, it tends to be very
generic - supporting only word to word match or regex-based patterns at
best. There is thus a need for an information retrieval system which
retrieves relevant data at a lower level.

INTRODUCTION 
-------------

As the availability of internet spread to the common masses, developers
slowly realized the need for a centralized system to keep track of the
tremendous amount of data made available online. To satisfy this need,
the first search engine Archie was made available in 1990. Search
engines have since developed exponentially to keep up with the growing
demands of users. The development of these search engines has left the
native in-browser search at a very basic level.

Although similar in functionality, the primary difference between the
two is that search engines work at the highest level, providing pages
for a query while in-browser search works at the lower level and matches
paragraphs or word for a query. Due to the difference in their use
cases, the two should not be discriminated and as such, for the sake of
efficiency and convenience, the development of one should take place
parallel to the other.

Fuzzy Search is an attempt to bridge this gap in development to provide
an optimized in-browser search functionality.

PROPOSED SYSTEM 
----------------

-   Fuzzy Find is a browser extension which can match words on a page
    with a search query based on certain filters.

-   The frontend consists of a simple text field which pops up on
    pressing a keyboard shortcut. The user can enter the query terms in
    this field.

-   As the user types terms into this field, they will be filtered and
    processed to generate terms similar to the input.

-   As these terms are generated, the open page will be scanned for
    their presence. If found, they will be highlighted on the page and
    brought into view.

PROPOSED ARCHITECTURE 
----------------------

![](media/image1.png)

A specific keyboard combination is used to launch a pop-up text
field where the user can enter a term or a list of terms. As the user
enters the terms, each of them will be passed to the processing script
where it will pass through a series of stages, depending on the user's
extension configuration. Each stage will generate and add new terms to
the result array.

After passing through all the processing stages, the result array will
be passed to the highlight script. The script will then search the
entire DOM for presence of terms from the result array. If any term
matches, it's background will be set to yellow to visually indicate its
presence.

Fuzzy Find is developed purely in vanilla JavaScript.

Features
------------------------

-   Matching words on a page that share meaning similar to the query.

-   Matching words on a page that have similar phonetic sound.
    \[Homophones\]

-   Find words that are spelled similar to the query words. \[Spell
    correction\]

-   Querying for words that are strongly related/describe the query
    phrase.

-   Searching for words that are related to the query and start with a
    particular letter.

-   Words that often follow a term and start with a particular letter.

Usage
------------------------

-   Press Alt + Shift + F and enter any query to search.

-   Extension performs spell correction and searches for similar and phonetic words by default.

-   Tap the three-dot menu to explore the remaining options.


**Note:** The major functionality for this extension is from the datamuse API. Explore it for yourself at https://www.datamuse.com/api/. Also, please note that the extension does not log ANY data.
