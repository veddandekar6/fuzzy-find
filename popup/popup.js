window.onload = function () {
  // Bring into view (curr + 1)th element
  function nextResult() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, ["next", curr], function (response) {
        if (response) curr = response;
      });
    });
  }

  // Bring into view (curr - 1)th element
  function prevResult() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, ["prev", curr], function (response) {
        if (response) curr = response;
      });
    });
  }

  // Grab values from all input fields
  function update() {
    let search = document.getElementById("search").value;
    if (search == "") return;
    let similar = document.getElementById("similar").checked;
    let homophones = document.getElementById("homophones").checked;
    let rhyming = document.getElementById("rhyming").checked;
    let saw = document.getElementById("saw").checked;
    let similarStart = document.getElementById("similarStart").checked;
    let similarStartText = document.getElementById("rStartingBox").value;
    let followingQuery = document.getElementById("followingQuery").checked;
    let followingQueryText = document.getElementById("fStartingBox").value;

    // Compile data
    let data = {
      data: search,
      similar: similar,
      homophones: homophones,
      rhyming: rhyming,
      saw: saw,
      similarStart: similarStart,
      similarStartText: similarStartText,
      followingQuery: followingQuery,
      followingQueryText: followingQueryText,
    };

    // Send data to content script
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, data, function (response) {
        if (response == "") {
          curr = -1;
          nextResult();
        }
      });
    });
  }

  // Wait for user to finish typing input
  function inputTimeout() {
    clearTimeout(timerId);
    timerId = setTimeout(update, interval);
  }

  // Inject page with the required content scripts using background script
  const bgScript = chrome.extension.getBackgroundPage();
  bgScript.inject();

  // Timer identified and wait interval
  let timerId;
  const interval = 1000;

  // Currently highlighted element
  let curr = 0;

  // Add input handlers to all input fields
  document.getElementById("search").addEventListener("keyup", inputTimeout);
  document
    .getElementById("rStartingBox")
    .addEventListener("keyup", inputTimeout);
  document
    .getElementById("fStartingBox")
    .addEventListener("keyup", inputTimeout);
  document.getElementById("similar").addEventListener("change", inputTimeout);
  document
    .getElementById("homophones")
    .addEventListener("change", inputTimeout);
  document.getElementById("rhyming").addEventListener("change", inputTimeout);
  document.getElementById("saw").addEventListener("change", inputTimeout);
  document
    .getElementById("similarStart")
    .addEventListener("change", inputTimeout);
  document
    .getElementById("followingQuery")
    .addEventListener("change", inputTimeout);

  // Add show and hide functionality to advanced button and checkboxes
  document.getElementById("advanced_btn").addEventListener("click", () => {
    let advancedPanel = document.getElementById("advancedPanel");
    advancedPanel.style.display =
      advancedPanel.style.display == "block" ? "none" : "block";
  });
  document.getElementById("similarStart").addEventListener("change", () => {
    let rsb = document.getElementById("rStartingBox");
    rsb.style.display = rsb.style.display == "block" ? "none" : "block";
  });
  document.getElementById("followingQuery").addEventListener("change", () => {
    let fsb = document.getElementById("fStartingBox");
    fsb.style.display = fsb.style.display == "block" ? "none" : "block";
  });

  // Add functionality to next and previous result button
  document.getElementById("upArrow").addEventListener("click", prevResult);
  document.getElementById("downArrow").addEventListener("click", nextResult);

  // Add next and previous functionality to keyboard presses
  document
    .getElementById("search")
    .addEventListener("keydown", function (event) {
      if (event.keyCode == 13) nextResult();
    });
};
