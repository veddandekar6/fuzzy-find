//Uses mark.js to highlight each word from array of words passed in argument
//Index as follows
//0 => typed words
//1 => spell corrected words
//2 => Similar words
//3 => Homophones
//4 => Rhyming words
//5 => Strongly associated words
//6 => Words related to query and starting with
//7 => Words that follow query and starting with
function markWords(words) {
  const options = {
    separateWordSearch: false,
    ignoreJoiners: true,
    accuracy: "complementary",
    ignorePunctuation: ":;.,-–—‒_(){}[]!'\"+=".split(""),
  };

  let instance = new Mark(document.querySelector("body"));
  //To remove highlights from previous search
  instance.unmark();

  // Set used to keep only unique values
  let toMark = new Set();

  for (let i = 0; i < words.length; i++)
    for (let j = 0; j < words[i].length && j < 15; j++) {
      if (i == 0 || i == 1) toMark.add(words[i][j]);
      else toMark.add(words[i][j].word);
    }
  instance.mark(toMark, options);
}

//Returns object containing words that often follow query and begining with something particular
function followQueryStartingWith(words, begin, checked) {
  if (!checked) return new Promise((resolve) => resolve(Array()));
  let query = "";
  return new Promise((resolve, reject) => {
    words.forEach((word) => {
      query = query + word + "+";
    });
    let url =
      "https://api.datamuse.com/words?lc=" + query + "&sp=" + begin + "*";
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) resolve(JSON.parse(this.responseText));
        else
          reject(
            "Failed to fetch words starting with letter and follow a query"
          );
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

//Returns object containing words that are related with query and begining with something particular
function relatedQueryStartingWith(words, begin, checked) {
  if (!checked) return new Promise((resolve) => resolve(Array()));
  let query = "";
  return new Promise((resolve, reject) => {
    words.forEach((word) => {
      query = query + word + "+";
    });
    let url =
      "https://api.datamuse.com/words?ml=" + query + "&sp=" + begin + "*";
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) resolve(JSON.parse(this.responseText));
        else
          reject(
            "Failed to fetch words starting with letter and related to query"
          );
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

//Returns object containing words that are strongly associated with the argument
function stronglyAssociatedWords(words, checked) {
  if (!checked) return new Promise((resolve) => resolve(Array()));
  return new Promise((resolve, reject) => {
    let word = words[0];
    let url = "https://api.datamuse.com/words?rel_trg=" + word;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) resolve(JSON.parse(this.responseText));
        else reject("Failed to fetch homophones");
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

//Returns object containing words that rhymes with the argument
function rhymingWords(words, checked) {
  if (!checked) return new Promise((resolve) => resolve(Array()));
  return new Promise((resolve, reject) => {
    let word = words[0];
    let url = "https://api.datamuse.com/words?rel_rhy=" + word;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) resolve(JSON.parse(this.responseText));
        else reject("Failed to fetch homophones");
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

//Returns object containing words that sound similar to argument
function homophones(words, checked) {
  if (!checked) return new Promise((resolve) => resolve(Array()));
  return new Promise((resolve, reject) => {
    let word = words[0];
    let url = "https://api.datamuse.com/words?sl=" + word;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) resolve(JSON.parse(this.responseText));
        else reject("Failed to fetch homophones");
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

//Returns object containing words/phrases having meaning similar to argument
function similarWords(words, checked) {
  if (!checked) return new Promise((resolve) => resolve(Array()));
  return new Promise((resolve, reject) => {
    let query = "";
    words.forEach((word) => {
      query += word + "+";
    });
    let url = "https://api.datamuse.com/words?ml=" + query;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) resolve(JSON.parse(this.responseText));
        else reject("Failed to fetch similar meanings");
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

//Returns spell-corrected word from argument
function spellCheck(words) {
  return new Promise((resolve, reject) => {
    let url = "https://api.datamuse.com/words?sp=" + words;
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          if (JSON.parse(this.responseText).length != 0) {
            resolve(JSON.parse(this.responseText)[0].word);
          } else {
            resolve(words);
          }
        } else reject("Failed to fetch correct spelling");
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  });
}

chrome.runtime.onMessage.addListener(function (data, sender, sendResponse) {
  if (!data || Array.isArray(data)) return;
  //Perform word by word spell correction
  let promises = data.data.split(" ").map(spellCheck);
  Promise.all(promises).then((spellCorrectedWords) => {
    //Perform all fetches
    Promise.all([
      similarWords(spellCorrectedWords, data.similar),
      homophones(spellCorrectedWords, data.homophones),
      rhymingWords(spellCorrectedWords, data.rhyming),
      stronglyAssociatedWords(spellCorrectedWords, data.saw),
      relatedQueryStartingWith(
        spellCorrectedWords,
        data.similarStartText,
        data.similarStart
      ),
      followQueryStartingWith(
        spellCorrectedWords,
        data.followingQueryText,
        data.followingQuery
      ),
    ])
      .then((results) => {
        // Add user typed word and spell corrected words into results to be marked
        results.unshift(data.data.split(" "), spellCorrectedWords);
        //Highlight results in document
        markWords(results);
      })
      .then(() => sendResponse("marked"));
  });
  return true;
});
