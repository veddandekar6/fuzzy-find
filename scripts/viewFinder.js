chrome.runtime.onMessage.addListener(function (data, sender, sendResponse) {
  if (!data || !Array.isArray(data)) return;
  if (data[0] == "next") {
    // Call from nextResult

    marked = document.getElementsByTagName("mark");
    // Un-highlight previous highlight, if any
    if (marked[data[1]]) marked[data[1]].style.backgroundColor = "yellow";
    // New highlight
    let curr = (data[1] + 1) % marked.length;
    marked[curr].scrollIntoView({
      behavior: "auto",
      block: "center",
      inline: "center",
    });
    marked[curr].style.backgroundColor = "blue";

    sendResponse(curr);
  } else if (data[0] == "prev") {
    // Call from prevResult

    marked = document.getElementsByTagName("mark");
    // Un-highlight previous highlight, if any
    if (marked[data[1]]) marked[data[1]].style.backgroundColor = "yellow";
    // New highlight
    let curr = (data[1] - 1) % marked.length;
    if (curr == -1) curr = marked.length - 1;
    marked[curr].scrollIntoView({
      behavior: "auto",
      block: "center",
      inline: "center",
    });
    marked[curr].style.backgroundColor = "blue";

    sendResponse(curr);
  }
});
